function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').html(sporocilo);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    var wink ="<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png'/>";
    var smiley ="<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png'/>";
    var like ="<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png'/>";
    var kiss ="<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png'/>";
    var sad ="<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png'/>";
    sporocilo = sporocilo.replace(/;\)/g, wink);
    sporocilo = sporocilo.replace(/:\)/g, smiley);
    sporocilo = sporocilo.replace(/\(y\)/g, like);
    sporocilo = sporocilo.replace(/:\*/g, kiss);
    sporocilo = sporocilo.replace(/:\(/g, sad);
    klepetApp.posljiSporocilo($('#kanal').text().split(' @ ')[1], sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  var imeVz, imeK;

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
      imeVz = rezultat.vzdevek;
      $('#kanal').text(rezultat.vzdevek + " @ " + imeK);
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    imeK = rezultat.kanal;
    $('#kanal').text(imeVz + " @ " + rezultat.kanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  socket.on('sporocilo', function (sporocilo) {
    var novElement = $('<div style="font-weight: bold"></div>').html(sporocilo.besedilo);
    $('#sporocila').append(novElement);
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  setInterval(function() {
    socket.emit('kanali');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});